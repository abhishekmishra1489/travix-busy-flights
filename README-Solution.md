# Travix - Solution


### Why below files are created ?
* `FlightResilience`  - to take care of fallback scenario . 
For example if one of the apis (ToughJet-api) is not available , we dont want the application to break rather respond back  CrazyAir-api response or vice-versa .
if both of the apis are not available an empty response is sent .

* `LoggingAspect` - will help to log every method of respective class that have been traversed . This helps to see detailed log in production and figure out the failed method very quickly .
* `ControllerAdvisor` - used for centralized logging for customising own exception . 

* `Interface CrazyAirServiceProxy` - will have the end point to connect to `CrazyAir-api` via feign Client .
* `Interface ToughJetServiceProxy` - will have the end point to connect to `CrazyAir-api` via feign Client .
* `Interface FlightService` - will help all suppliers to 
List<FlightResponseDTO> fetchFlightDetails(FlightRequestDTO flightRequestDTO)

### Changes in model
 - `DepartureDate` and `ReturnDate` attribute type had been updated to `LocalDateTime` .  Validating the travel dates across world needs to be of ISO_LOCAL_Date type.
 - `origin` and `destination` attribute type has been updated to `AirportCodeEnum`. This helped to maintain a similar 3 letter IATA code across .


##### With few  steps, another supplier(e.g [Wao-Sky]()) can be added to `BusyFlights`  - .
* Create [WaoSkyService]()  and extend `FlightService` . Implement the method `fetchFlightDetails()` 
which takes in `FlightRequestDTO` and returns `List<FlightResponseDTO>`
* Add [WaoSkyService]() to `FlightServiceFacade`
* Create [WaoSkyServiceProxy]() and mention the endpoint to connect to [WaoSky-Api]()
* Add a fallback scenario of [WaoSky-Api]() in `FlightResilience`

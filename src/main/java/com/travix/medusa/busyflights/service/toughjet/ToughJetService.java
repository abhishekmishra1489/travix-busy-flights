package com.travix.medusa.busyflights.service.toughjet;

import com.travix.medusa.busyflights.circuitbreaker.FlightResilience;
import com.travix.medusa.busyflights.domain.FlightRequestDTO;
import com.travix.medusa.busyflights.domain.FlightResponseDTO;
import com.travix.medusa.busyflights.domain.toughjet.ToughJetRequest;
import com.travix.medusa.busyflights.domain.toughjet.ToughJetResponseList;
import com.travix.medusa.busyflights.helper.toughjet.ToughJetServiceHelper;
import com.travix.medusa.busyflights.service.FlightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ToughJetService implements FlightService {

    private final FlightResilience flightResilience;
    private final ToughJetServiceHelper toughJetServiceHelper;

    @Autowired
    public ToughJetService(FlightResilience flightResilience, ToughJetServiceHelper toughJetServiceHelper) {
        this.flightResilience = flightResilience;
        this.toughJetServiceHelper = toughJetServiceHelper;
    }

    @Override
    public List<FlightResponseDTO> fetchFlightDetails(FlightRequestDTO flightRequestDTO) {
        ToughJetRequest toughJetRequest
                = toughJetServiceHelper.mapFlightRequestDtoToToughJetRequest(flightRequestDTO);
        ToughJetResponseList toughJetResponseList
                = flightResilience.callToughJetApi(toughJetRequest);
        return toughJetResponseList
                .getToughJetResponseList()
                .stream()
                .map(toughJetServiceHelper::mapToughJetResponseToFlightResponseDto)
                .collect(Collectors.toList());
    }
}

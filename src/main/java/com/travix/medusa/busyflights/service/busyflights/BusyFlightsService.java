package com.travix.medusa.busyflights.service.busyflights;

import com.travix.medusa.busyflights.controller.request.BusyFlightsRequest;
import com.travix.medusa.busyflights.controller.response.BusyFlightsResponseList;
import com.travix.medusa.busyflights.domain.FlightRequestDTO;
import com.travix.medusa.busyflights.domain.FlightResponseDTO;
import com.travix.medusa.busyflights.helper.busyflights.BusyFlightsServiceHelper;
import com.travix.medusa.busyflights.service.FlightServiceFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BusyFlightsService {

    final FlightServiceFacade flightServiceFacade;
    final BusyFlightsServiceHelper busyFlightsServiceHelper;

    @Autowired
    public BusyFlightsService(FlightServiceFacade flightServiceFacade, BusyFlightsServiceHelper busyFlightsServiceHelper) {
        this.flightServiceFacade = flightServiceFacade;
        this.busyFlightsServiceHelper = busyFlightsServiceHelper;
    }

    public BusyFlightsResponseList getAggregatedFlightSearchResults(BusyFlightsRequest busyFlightsRequest) {
        busyFlightsServiceHelper.validateReturnDateIsAfterDepartureDate(busyFlightsRequest);
        FlightRequestDTO flightRequestDTO
                = busyFlightsServiceHelper.mapBusyFlightRequestToFlightRequestDto(busyFlightsRequest);
        List<FlightResponseDTO> flightResponseDTOS
                = flightServiceFacade.getFlightResponses(flightRequestDTO);
        busyFlightsServiceHelper.sortByFareAscending(flightResponseDTOS);
        return busyFlightsServiceHelper.mapFlightResponseDtoToBusyFlightResponseList(flightResponseDTOS);
    }

}

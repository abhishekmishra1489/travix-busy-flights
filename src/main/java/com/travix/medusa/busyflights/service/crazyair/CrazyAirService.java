package com.travix.medusa.busyflights.service.crazyair;

import com.travix.medusa.busyflights.circuitbreaker.FlightResilience;
import com.travix.medusa.busyflights.domain.FlightRequestDTO;
import com.travix.medusa.busyflights.domain.FlightResponseDTO;
import com.travix.medusa.busyflights.domain.crazyair.CrazyAirRequest;
import com.travix.medusa.busyflights.domain.crazyair.CrazyAirResponseList;
import com.travix.medusa.busyflights.helper.crazyair.CrazyAirServiceHelper;
import com.travix.medusa.busyflights.service.FlightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CrazyAirService implements FlightService {

    private final FlightResilience flightResilience;
    private final CrazyAirServiceHelper crazyAirServiceHelper;

    @Autowired
    public CrazyAirService(FlightResilience flightResilience, CrazyAirServiceHelper crazyAirServiceHelper) {
        this.flightResilience = flightResilience;
        this.crazyAirServiceHelper = crazyAirServiceHelper;
    }

    @Override
    public List<FlightResponseDTO> fetchFlightDetails(FlightRequestDTO flightRequestDTO) {
        CrazyAirRequest crazyAirRequest
                = crazyAirServiceHelper.mapFlightRequestDtoToCrazyAirRequest(flightRequestDTO);
        CrazyAirResponseList crazyAirResponseList
                = flightResilience.callCrazyAirApi(crazyAirRequest);
        return crazyAirResponseList
                .getCrazyAirResponseList()
                .stream()
                .map(crazyAirServiceHelper::mapCrazyAirResponseToFlightResponseDto)
                .collect(Collectors.toList());
    }
}

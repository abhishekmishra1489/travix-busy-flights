package com.travix.medusa.busyflights.service;

import com.travix.medusa.busyflights.domain.FlightRequestDTO;
import com.travix.medusa.busyflights.domain.FlightResponseDTO;
import com.travix.medusa.busyflights.service.crazyair.CrazyAirService;
import com.travix.medusa.busyflights.service.toughjet.ToughJetService;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class FlightServiceFacade {

    private final CrazyAirService crazyAirService;
    private final ToughJetService toughJetService;

    public FlightServiceFacade(CrazyAirService crazyAirService, ToughJetService toughJetService) {
        this.crazyAirService = crazyAirService;
        this.toughJetService = toughJetService;
    }

    public List<FlightResponseDTO> getFlightResponses(FlightRequestDTO flightRequestDTO) {
        List<FlightService> flightServices = Arrays.asList(crazyAirService, toughJetService);
        return flightServices.
                parallelStream()
                .map(flightService -> {
                    return flightService.fetchFlightDetails(flightRequestDTO);
                })
                .flatMap(flightDetailsList -> flightDetailsList.stream())
                .collect(Collectors.toList());
    }
}

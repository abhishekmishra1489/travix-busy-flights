package com.travix.medusa.busyflights.service;

import com.travix.medusa.busyflights.domain.FlightResponseDTO;
import com.travix.medusa.busyflights.domain.FlightRequestDTO;

import java.util.List;

public interface FlightService {
    List<FlightResponseDTO> fetchFlightDetails(FlightRequestDTO flightRequestDTO) ;
}

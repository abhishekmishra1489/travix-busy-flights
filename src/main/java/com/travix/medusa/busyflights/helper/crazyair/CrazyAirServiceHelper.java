package com.travix.medusa.busyflights.helper.crazyair;

import com.travix.medusa.busyflights.domain.FlightRequestDTO;
import com.travix.medusa.busyflights.domain.FlightResponseDTO;
import com.travix.medusa.busyflights.domain.crazyair.CrazyAirRequest;
import com.travix.medusa.busyflights.domain.crazyair.CrazyAirResponse;
import org.springframework.stereotype.Component;

@Component
public class CrazyAirServiceHelper {

    public CrazyAirRequest mapFlightRequestDtoToCrazyAirRequest(FlightRequestDTO flightRequestDTO) {
        CrazyAirRequest crazyAirRequest =
                CrazyAirRequest.builder()
                        .origin(flightRequestDTO.getOrigin())
                        .destination(flightRequestDTO.getDestination())
                        .departureDate(flightRequestDTO.getDepartureDate())
                        .returnDate(flightRequestDTO.getReturnDate())
                        .passengerCount(flightRequestDTO.getNumberOfPassengers())
                        .build();
        return crazyAirRequest;
    }

    public FlightResponseDTO mapCrazyAirResponseToFlightResponseDto(CrazyAirResponse crazyAirResponse) {
        return FlightResponseDTO.builder()
                .airline(crazyAirResponse.getAirline())
                .supplier("Crazy Air")
                .fare(crazyAirResponse.getPrice())
                .destinationAirportCode(crazyAirResponse.getDestinationAirportCode())
                .departureAirportCode(crazyAirResponse.getDepartureAirportCode())
                .departureDate(crazyAirResponse.getDepartureDate())
                .arrivalDate(crazyAirResponse.getArrivalDate())
                .build();
    }
}

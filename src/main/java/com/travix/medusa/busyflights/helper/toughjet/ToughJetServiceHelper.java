package com.travix.medusa.busyflights.helper.toughjet;

import com.travix.medusa.busyflights.domain.FlightRequestDTO;
import com.travix.medusa.busyflights.domain.FlightResponseDTO;
import com.travix.medusa.busyflights.domain.toughjet.ToughJetRequest;
import com.travix.medusa.busyflights.domain.toughjet.ToughJetResponse;
import org.springframework.stereotype.Component;

@Component
public class ToughJetServiceHelper {

    public ToughJetRequest mapFlightRequestDtoToToughJetRequest(FlightRequestDTO flightRequestDTO) {
        return ToughJetRequest.builder()
                .from(flightRequestDTO.getOrigin())
                .to(flightRequestDTO.getDestination())
                .outboundDate(flightRequestDTO.getDepartureDate())
                .inboundDate(flightRequestDTO.getReturnDate())
                .numberOfAdults(flightRequestDTO.getNumberOfPassengers())
                .build();
    }

    public FlightResponseDTO mapToughJetResponseToFlightResponseDto(ToughJetResponse toughJetResponse) {
        return FlightResponseDTO.builder()
                .airline(toughJetResponse.getCarrier())
                .supplier("Tough Jet")
                .fare(toughJetResponse.getBasePrice())
                .destinationAirportCode(toughJetResponse.getArrivalAirportName())
                .departureAirportCode(toughJetResponse.getDepartureAirportName())
                .departureDate(toughJetResponse.getOutboundDateTime())
                .arrivalDate(toughJetResponse.getInboundDateTime())
                .build();
    }
}

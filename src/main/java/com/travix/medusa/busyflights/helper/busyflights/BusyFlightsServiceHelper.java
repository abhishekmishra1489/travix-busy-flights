package com.travix.medusa.busyflights.helper.busyflights;

import com.travix.medusa.busyflights.controller.request.BusyFlightsRequest;
import com.travix.medusa.busyflights.controller.response.BusyFlightsResponse;
import com.travix.medusa.busyflights.controller.response.BusyFlightsResponseList;
import com.travix.medusa.busyflights.domain.FlightRequestDTO;
import com.travix.medusa.busyflights.domain.FlightResponseDTO;
import com.travix.medusa.busyflights.exception.BusyFlightsException;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class BusyFlightsServiceHelper {

    public void validateReturnDateIsAfterDepartureDate(BusyFlightsRequest busyFlightsRequest) {
        if (busyFlightsRequest.getReturnDate().isBefore(busyFlightsRequest.getDepartureDate())
                || busyFlightsRequest.getReturnDate().isEqual(busyFlightsRequest.getDepartureDate())) {
            throw new BusyFlightsException("returnDate should be after departureDate");
        }
    }

    public FlightRequestDTO mapBusyFlightRequestToFlightRequestDto(BusyFlightsRequest busyFlightsRequest) {
        return FlightRequestDTO
                .builder()
                .departureDate(busyFlightsRequest.getDepartureDate())
                .returnDate(busyFlightsRequest.getReturnDate())
                .origin(busyFlightsRequest.getOrigin())
                .destination(busyFlightsRequest.getDestination())
                .numberOfPassengers(busyFlightsRequest.getNumberOfPassengers())
                .build();
    }

    public void sortByFareAscending(List<FlightResponseDTO> flightResponseDTOS) {
        flightResponseDTOS.sort((o1, o2) -> o1.getFare() > o2.getFare() ? 1 : -1);
    }

    public BusyFlightsResponseList mapFlightResponseDtoToBusyFlightResponseList(List<FlightResponseDTO> flightResponseDTOS) {
        return BusyFlightsResponseList
                .builder()
                .busyFlightsResponseList(flightResponseDTOS
                        .stream()
                        .map(this::mapFlightResponseDtoToBusyFlightsResponse)
                        .collect(Collectors.toList()))
                .build();
    }

    public BusyFlightsResponse mapFlightResponseDtoToBusyFlightsResponse(FlightResponseDTO flightResponseDTO) {
        return BusyFlightsResponse
                .builder()
                .airline(flightResponseDTO.getAirline())
                .arrivalDate(flightResponseDTO.getArrivalDate())
                .destinationAirportCode(flightResponseDTO.getDestinationAirportCode())
                .departureAirportCode(flightResponseDTO.getDepartureAirportCode())
                .departureDate(flightResponseDTO.getDepartureDate())
                .fare(flightResponseDTO.getFare())
                .supplier(flightResponseDTO.getSupplier())
                .build();
    }
}

package com.travix.medusa.busyflights.domain;

import com.travix.medusa.busyflights.domain.enums.AirportCodeEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class FlightRequestDTO {

    private AirportCodeEnum origin, destination;
    private LocalDateTime departureDate, returnDate;
    private int numberOfPassengers;

}

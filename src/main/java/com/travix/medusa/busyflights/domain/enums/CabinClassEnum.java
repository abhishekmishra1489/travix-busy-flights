package com.travix.medusa.busyflights.domain.enums;

public enum CabinClassEnum {
    E("ECONOMY CLASS"),
    B("BUSINESS CLASS");

    String value;

    CabinClassEnum(String value) {
        this.value = value;
    }

}

package com.travix.medusa.busyflights.domain.toughjet;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ToughJetResponseList {
    private List<ToughJetResponse> toughJetResponseList;
}

package com.travix.medusa.busyflights.domain.enums;

public enum AirportCodeEnum {
    LHR("LONDON"),
    AMS("AMSTERDAM"),
    MUC("MUNICH"),
    ZSH("ZURICH"),
    NCE("NICE");

    String value;

    AirportCodeEnum(String value) {
        this.value = value;
    }

}

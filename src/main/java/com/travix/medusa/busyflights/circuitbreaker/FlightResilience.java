package com.travix.medusa.busyflights.circuitbreaker;

import com.travix.medusa.busyflights.domain.crazyair.CrazyAirRequest;
import com.travix.medusa.busyflights.domain.crazyair.CrazyAirResponse;
import com.travix.medusa.busyflights.domain.crazyair.CrazyAirResponseList;
import com.travix.medusa.busyflights.domain.toughjet.ToughJetRequest;
import com.travix.medusa.busyflights.domain.toughjet.ToughJetResponse;
import com.travix.medusa.busyflights.domain.toughjet.ToughJetResponseList;
import com.travix.medusa.busyflights.feign.CrazyAirServiceProxy;
import com.travix.medusa.busyflights.feign.ToughJetServiceProxy;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@Slf4j
public class FlightResilience {

    private final CrazyAirServiceProxy crazyAirServiceProxy;
    private final ToughJetServiceProxy toughJetServiceProxy;

    @Autowired
    public FlightResilience(CrazyAirServiceProxy crazyAirServiceProxy, ToughJetServiceProxy toughJetServiceProxy) {
        this.crazyAirServiceProxy = crazyAirServiceProxy;
        this.toughJetServiceProxy = toughJetServiceProxy;
    }

    @CircuitBreaker(name = "crazyAirApiCallResilienceService", fallbackMethod = "crazyAirApiCallFallback")
    public CrazyAirResponseList callCrazyAirApi(CrazyAirRequest crazyAirRequest) {
        return crazyAirServiceProxy
                .fetchCrazyAirFlightDetails(crazyAirRequest);
    }

    public CrazyAirResponseList crazyAirApiCallFallback(CrazyAirRequest crazyAirRequest, Throwable throwable) {
        logger.debug("Crazy-Air-Api call failed with exception - {} ", throwable.getCause());
        List<CrazyAirResponse> crazyAirResponses = new ArrayList<>();
        return CrazyAirResponseList
                .builder()
                .crazyAirResponseList(crazyAirResponses)
                .build();
    }

    @CircuitBreaker(name = "toughJetApiCallResilienceService", fallbackMethod = "toughJetApiCallFallback")
    public ToughJetResponseList callToughJetApi(ToughJetRequest toughJetRequest) {
        return toughJetServiceProxy
                .fetchToughJetFlightDetails(toughJetRequest);
    }

    public ToughJetResponseList toughJetApiCallFallback(ToughJetRequest toughJetRequest, Throwable throwable) {
        logger.info("Tough-Jet-Api call failed with exception - {} ", throwable.getCause());
        List<ToughJetResponse> toughJetResponses = new ArrayList<>();
        return ToughJetResponseList.builder()
                .toughJetResponseList(toughJetResponses)
                .build();
    }


}

package com.travix.medusa.busyflights.logging;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Aspect
@Component
@Slf4j
public class LoggingAspect {

    @Pointcut("(within(@org.springframework.stereotype.Component *) || " +
            "within(@org.springframework.stereotype.Service *) || " +
            "within(@org.springframework.web.bind.annotation.RestController *)) && " +
            "within(com.travix.medusa.busyflights..*)")

    public void beanAnnotatedWithServiceAnnotation() {
        // just needs this method to be invoked no implementation required
    }

    @Around("beanAnnotatedWithServiceAnnotation()")
    public Object applicationLogger(ProceedingJoinPoint joinPoint) throws Throwable {
        Signature signature = joinPoint.getSignature();
        String declaringTypeName = signature.getDeclaringTypeName();
        String methodName = signature.getName();
        Object[] objects = joinPoint.getArgs();
        String arguments = Arrays.toString(objects);
        logger.debug("Enter: {}.{}() with argument[s] = {}", declaringTypeName,
                methodName, arguments);
        Object result = joinPoint.proceed();
        logger.debug("Exit: {}.{}() with result = {}", declaringTypeName,
                methodName, result);
        return result;
    }
}
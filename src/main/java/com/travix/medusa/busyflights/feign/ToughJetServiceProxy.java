package com.travix.medusa.busyflights.feign;

import com.travix.medusa.busyflights.domain.toughjet.ToughJetRequest;
import com.travix.medusa.busyflights.domain.toughjet.ToughJetResponseList;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;


@FeignClient(name = "tough-jet-api")
@RibbonClient(name = "tough-jet-api")
public interface ToughJetServiceProxy {
    @PostMapping(path = "/tough-jet/flights-detail")
    ToughJetResponseList fetchToughJetFlightDetails(@RequestBody final ToughJetRequest toughJetRequest);
}



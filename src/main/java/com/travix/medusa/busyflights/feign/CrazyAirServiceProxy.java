package com.travix.medusa.busyflights.feign;

import com.travix.medusa.busyflights.domain.crazyair.CrazyAirRequest;
import com.travix.medusa.busyflights.domain.crazyair.CrazyAirResponseList;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "crazy-air-api")
@RibbonClient(name = "crazy-air-api")
public interface CrazyAirServiceProxy {
    @PostMapping(path = "/crazy-air/flights-detail")
    CrazyAirResponseList fetchCrazyAirFlightDetails(@RequestBody final CrazyAirRequest crazyAirRequest);

}

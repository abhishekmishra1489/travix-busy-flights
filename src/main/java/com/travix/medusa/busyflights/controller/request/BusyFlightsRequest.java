package com.travix.medusa.busyflights.controller.request;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import com.travix.medusa.busyflights.domain.enums.AirportCodeEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BusyFlightsRequest {

    @NotNull(message="Please Enter three letter IATA Code for origin (eg. LHR, AMS)")
    private AirportCodeEnum origin;

    @NotNull(message="Please Enter three letter IATA Code for destination (eg. LHR, AMS)")
    private AirportCodeEnum destination;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @NotNull(message = "departureDate is not valid ")
    private LocalDateTime departureDate;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @NotNull(message = "returnDate is not valid ")
    private LocalDateTime returnDate;

    @Min(value = 1, message = "numberOfPassengers is not valid")
    private int numberOfPassengers;

}

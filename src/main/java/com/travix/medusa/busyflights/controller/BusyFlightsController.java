package com.travix.medusa.busyflights.controller;

import com.travix.medusa.busyflights.controller.request.BusyFlightsRequest;
import com.travix.medusa.busyflights.controller.response.BusyFlightsResponseList;
import com.travix.medusa.busyflights.service.busyflights.BusyFlightsService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.time.LocalDateTime;

@RestController
@Slf4j
public class BusyFlightsController {

    private static final String FLIGHTS = "/flights-search";
    private static final String CALCULATION_OF_TOOK_MS = "calculation of {} took: {} ms";

    private BusyFlightsService busyFlightsService;

    @Autowired
    public BusyFlightsController(BusyFlightsService busyFlightsService) {
        this.busyFlightsService = busyFlightsService;
    }

    @ApiOperation(
            value = "BusyFlights is a flights search solution which aggregates flight results from  different suppliers",
            notes = "will return the aggregated successful response from suppliers and only log the failure responses",
            response = BusyFlightsResponseList.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = BusyFlightsResponseList.class),
            @ApiResponse(code = 500, message = "Failure")})
    @PostMapping(FLIGHTS)
    public ResponseEntity<?> getFlightSearchResult(@Valid @RequestBody BusyFlightsRequest busyFlightsRequest) {
        LocalDateTime start = LocalDateTime.now();
        BusyFlightsResponseList busyFlightsResponseList = busyFlightsService.getAggregatedFlightSearchResults(busyFlightsRequest);
        logger.info(CALCULATION_OF_TOOK_MS, "BusyFlights", LocalDateTime.from(start).getMinute());
        return new ResponseEntity(busyFlightsResponseList, HttpStatus.OK);
    }

}

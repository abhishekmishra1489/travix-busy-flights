package com.travix.medusa.busyflights.controller.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BusyFlightsResponseList {
    private List<BusyFlightsResponse> busyFlightsResponseList ;
}

package com.travix.medusa.busyflights.exception;

public class CrazyAirException extends RuntimeException {

    public CrazyAirException(String msg) {
        super(msg);
    }

}

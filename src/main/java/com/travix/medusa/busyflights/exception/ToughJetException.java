package com.travix.medusa.busyflights.exception;

public class ToughJetException extends RuntimeException {

    public ToughJetException(String msg) {
        super(msg);
    }

}

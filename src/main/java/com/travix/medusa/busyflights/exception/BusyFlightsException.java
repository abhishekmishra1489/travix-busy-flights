package com.travix.medusa.busyflights.exception;

public class BusyFlightsException extends RuntimeException {

    public BusyFlightsException(String msg) {
        super(msg);
    }

}

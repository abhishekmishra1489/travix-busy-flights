package com.travix.medusa.busyflights.controller;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.travix.medusa.busyflights.controller.request.BusyFlightsRequest;
import com.travix.medusa.busyflights.domain.enums.AirportCodeEnum;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.LocalDateTime;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class BusyFlightsControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Before
    public void init() {
    }

    @Test
    public void getFlightSearchResult_IntegrationTest_failure() throws Exception {
        this.mockMvc
                .perform(post("/flights-search"))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void getFlightSearchResult_IntegrationTest_success() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.post("/flights-search")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(getBusyFlightsRequest())))
                .andExpect(status().isOk());
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private BusyFlightsRequest getBusyFlightsRequest() {
        LocalDateTime departureDate = LocalDateTime.of(2019, 2, 24, 4, 30);
        LocalDateTime arrivalDate = LocalDateTime.of(2019, 3, 25, 5, 30);
        return BusyFlightsRequest
                .builder()
                .departureDate(departureDate)
                .returnDate(arrivalDate)
                .origin(AirportCodeEnum.AMS)
                .destination(AirportCodeEnum.MUC)
                .numberOfPassengers(2)
                .build();
    }
}
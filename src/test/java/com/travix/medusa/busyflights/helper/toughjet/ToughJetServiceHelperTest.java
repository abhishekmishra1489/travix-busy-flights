package com.travix.medusa.busyflights.helper.toughjet;


import com.travix.medusa.busyflights.CommonTester;
import com.travix.medusa.busyflights.domain.FlightResponseDTO;
import com.travix.medusa.busyflights.domain.enums.AirportCodeEnum;
import com.travix.medusa.busyflights.domain.toughjet.ToughJetRequest;
import com.travix.medusa.busyflights.domain.toughjet.ToughJetResponse;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ToughJetServiceHelperTest {

    private ToughJetServiceHelper toughJetServiceHelper;
    private CommonTester commonTester;

    @Before
    public void init() {
        toughJetServiceHelper = new ToughJetServiceHelper();
        commonTester = new CommonTester();
    }

    @Test
    public void mapFlightRequestDtoToToughJetRequestTest_Success() {
        ToughJetRequest toughJetRequestActual = toughJetServiceHelper.mapFlightRequestDtoToToughJetRequest(commonTester.getFlightRequestDTO());
        Assert.assertEquals(getToughJetRequest(), toughJetRequestActual);
    }

    private ToughJetRequest getToughJetRequest() {
        return ToughJetRequest.builder()
                .from(AirportCodeEnum.AMS)
                .to(AirportCodeEnum.MUC)
                .outboundDate(CommonTester.DEPARTURE_DATE)
                .inboundDate(CommonTester.RETURN_DATE)
                .numberOfAdults(2)
                .build();
    }

    @Test
    public void mapToughJetResponseToFlightResponseDtoTest_Success() {

        FlightResponseDTO flightResponseDTOActual
                = toughJetServiceHelper.mapToughJetResponseToFlightResponseDto(commonTester.getToughJetResponse());
        assertEquals(commonTester.getFlightResponseDTOToughJet(), flightResponseDTOActual);
    }

}
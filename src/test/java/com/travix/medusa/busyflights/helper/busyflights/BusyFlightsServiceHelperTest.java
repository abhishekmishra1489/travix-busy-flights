package com.travix.medusa.busyflights.helper.busyflights;

import com.travix.medusa.busyflights.CommonTester;
import com.travix.medusa.busyflights.controller.request.BusyFlightsRequest;
import com.travix.medusa.busyflights.controller.response.BusyFlightsResponseList;
import com.travix.medusa.busyflights.domain.FlightRequestDTO;
import com.travix.medusa.busyflights.domain.FlightResponseDTO;
import com.travix.medusa.busyflights.exception.BusyFlightsException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class BusyFlightsServiceHelperTest {
    private BusyFlightsServiceHelper busyFlightsServiceHelper;
    private CommonTester commonTester;


    @Before
    public void init() {
        busyFlightsServiceHelper = new BusyFlightsServiceHelper();
        commonTester = new CommonTester();
    }

    @Test
    public void sortByFareAscendingTest_Success() {
        List<FlightResponseDTO> flightResponseDTOS = commonTester.getFlightResponseDTOsForSorting();
        busyFlightsServiceHelper.sortByFareAscending(flightResponseDTOS);
        Assert.assertTrue(flightResponseDTOS.get(0).getFare() == 420.23);
        Assert.assertTrue(flightResponseDTOS.get(1).getFare() == 423.23);
        Assert.assertTrue(flightResponseDTOS.get(2).getFare() == 423.29);
    }


    @Test(expected = BusyFlightsException.class)
    public void validateReturnDateIsAfterDepartureDateTest_BusyFlightsException() {
        LocalDateTime departureDateError = LocalDateTime.of(2019, 3, 24, 4, 30);
        LocalDateTime returnDateError = LocalDateTime.of(2019, 2, 25, 5, 30);
        BusyFlightsRequest busyFlightsRequest = commonTester.getBusyFlightsRequestWithIncorrectTravelDates(departureDateError, returnDateError);
        busyFlightsServiceHelper.validateReturnDateIsAfterDepartureDate(busyFlightsRequest);
    }

    @Test
    public void mapBusyFlightRequestToFlightRequestDtoTest_success() {
        FlightRequestDTO flightRequestDTOActual
                = busyFlightsServiceHelper.mapBusyFlightRequestToFlightRequestDto(commonTester.getBusyFlightsRequest());
        Assert.assertEquals(commonTester.getFlightRequestDTO(), flightRequestDTOActual);
    }

    @Test
    public void mapFlightResponseDtoToBusyFlightResponseListTest_success() {
        BusyFlightsResponseList busyFlightsResponseListActual
                = busyFlightsServiceHelper.mapFlightResponseDtoToBusyFlightResponseList(commonTester.getFlightResponseDTOsToughJet());
        assertNotNull(busyFlightsResponseListActual);
    }

}
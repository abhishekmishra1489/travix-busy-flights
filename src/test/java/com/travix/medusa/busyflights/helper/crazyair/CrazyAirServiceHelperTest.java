package com.travix.medusa.busyflights.helper.crazyair;


import com.travix.medusa.busyflights.CommonTester;
import com.travix.medusa.busyflights.domain.FlightResponseDTO;
import com.travix.medusa.busyflights.domain.crazyair.CrazyAirRequest;
import com.travix.medusa.busyflights.domain.crazyair.CrazyAirResponse;
import com.travix.medusa.busyflights.domain.enums.AirportCodeEnum;
import com.travix.medusa.busyflights.domain.enums.CabinClassEnum;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CrazyAirServiceHelperTest {

    private CrazyAirServiceHelper crazyAirServiceHelper ;
    private CommonTester commonTester;

    @Before
    public void init() {
        crazyAirServiceHelper = new CrazyAirServiceHelper();
        commonTester = new CommonTester();
    }

    @Test
    public void mapFlightRequestDtoToCrazyAirRequestTest_Success() {
        CrazyAirRequest crazyAirRequestActual
                = crazyAirServiceHelper.mapFlightRequestDtoToCrazyAirRequest(commonTester.getFlightRequestDTO());
        Assert.assertEquals(commonTester.getCrazyAirRequest() , crazyAirRequestActual);
    }



    @Test
    public void mapCrazyAirResponseToFlightResponseDtoTest_Success() {
        FlightResponseDTO flightResponseDTOActual
                = crazyAirServiceHelper.mapCrazyAirResponseToFlightResponseDto(commonTester.getCrazyAirResponse());
        Assert.assertEquals(commonTester.getFlightResponseDTOCrazyAir() , flightResponseDTOActual);
    }




}
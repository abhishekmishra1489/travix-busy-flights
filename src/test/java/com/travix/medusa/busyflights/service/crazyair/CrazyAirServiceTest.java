package com.travix.medusa.busyflights.service.crazyair;


import com.travix.medusa.busyflights.CommonTester;
import com.travix.medusa.busyflights.circuitbreaker.FlightResilience;
import com.travix.medusa.busyflights.domain.FlightRequestDTO;
import com.travix.medusa.busyflights.domain.FlightResponseDTO;
import com.travix.medusa.busyflights.domain.crazyair.CrazyAirRequest;
import com.travix.medusa.busyflights.domain.crazyair.CrazyAirResponse;
import com.travix.medusa.busyflights.domain.crazyair.CrazyAirResponseList;
import com.travix.medusa.busyflights.helper.crazyair.CrazyAirServiceHelper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CrazyAirServiceTest {
    private CrazyAirService crazyAirService;
    private FlightResilience flightResilience;
    private CrazyAirServiceHelper crazyAirServiceHelper;
    private CommonTester commonTester;

    @Before
    public void init() {
        flightResilience = mock(FlightResilience.class);
        crazyAirServiceHelper = mock(CrazyAirServiceHelper.class);
        crazyAirService = new CrazyAirService(flightResilience, crazyAirServiceHelper);
        commonTester = new CommonTester();
    }

    @Test
    public void fetchFlightDetailsTest_Success() {
        FlightResponseDTO flightResponseDTO = commonTester.getFlightResponseDTOCrazyAir();
        doReturn(mock(CrazyAirRequest.class))
                .when(crazyAirServiceHelper)
                .mapFlightRequestDtoToCrazyAirRequest(any(FlightRequestDTO.class));
        doReturn(commonTester.getCrazyAirResponseList())
                .when(flightResilience)
                .callCrazyAirApi(any(CrazyAirRequest.class));
        doReturn(flightResponseDTO)
                .when(crazyAirServiceHelper)
                .mapCrazyAirResponseToFlightResponseDto(any(CrazyAirResponse.class));
        List<FlightResponseDTO> flightResponseDTOsActual
                = crazyAirService.fetchFlightDetails(commonTester.getFlightRequestDTO());

        Assert.assertEquals(Arrays.asList(flightResponseDTO),flightResponseDTOsActual);
    }
}
package com.travix.medusa.busyflights.service.busyflights;


import com.travix.medusa.busyflights.CommonTester;
import com.travix.medusa.busyflights.controller.request.BusyFlightsRequest;
import com.travix.medusa.busyflights.controller.response.BusyFlightsResponseList;
import com.travix.medusa.busyflights.domain.FlightRequestDTO;
import com.travix.medusa.busyflights.domain.FlightResponseDTO;
import com.travix.medusa.busyflights.helper.busyflights.BusyFlightsServiceHelper;
import com.travix.medusa.busyflights.service.FlightServiceFacade;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;


@RunWith(SpringRunner.class)
@SpringBootTest
public class BusyFlightsServiceTest {

    private BusyFlightsService busyFlightsService;
    private CommonTester commonTester;
    private BusyFlightsServiceHelper busyFlightsServiceHelper;
    private FlightServiceFacade flightServiceFacade;

    @Before
    public void init() {
        flightServiceFacade = mock(FlightServiceFacade.class);
        busyFlightsServiceHelper = mock(BusyFlightsServiceHelper.class);
        busyFlightsService = new BusyFlightsService(flightServiceFacade, busyFlightsServiceHelper);
        commonTester = new CommonTester();
    }

    @Test
    public void getAggregatedFlightSearchResultsTest_Success() {
        BusyFlightsResponseList busyFlightsResponseListSortedByPrice
                = commonTester.getBusyFlightsResponseListSortedByPrice();
        doNothing()
                .when(busyFlightsServiceHelper)
                .validateReturnDateIsAfterDepartureDate(any(BusyFlightsRequest.class));
        doReturn(mock(FlightRequestDTO.class))
                .when(busyFlightsServiceHelper)
                .mapBusyFlightRequestToFlightRequestDto(ArgumentMatchers.any(BusyFlightsRequest.class));
        doReturn(mock(List.class))
                .when(flightServiceFacade)
                .getFlightResponses(any(FlightRequestDTO.class));
        doNothing()
                .when(busyFlightsServiceHelper)
                .sortByFareAscending(anyList());
        doReturn(busyFlightsResponseListSortedByPrice)
                .when(busyFlightsServiceHelper)
                .mapFlightResponseDtoToBusyFlightResponseList(anyList());
        BusyFlightsResponseList aggregatedFlightSearchResultsActual
                = busyFlightsService.getAggregatedFlightSearchResults(commonTester.getBusyFlightsRequest());
        assertEquals(busyFlightsResponseListSortedByPrice, aggregatedFlightSearchResultsActual);
    }
}
package com.travix.medusa.busyflights.service;

import com.travix.medusa.busyflights.CommonTester;
import com.travix.medusa.busyflights.domain.FlightRequestDTO;
import com.travix.medusa.busyflights.domain.FlightResponseDTO;
import com.travix.medusa.busyflights.service.crazyair.CrazyAirService;
import com.travix.medusa.busyflights.service.toughjet.ToughJetService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class FlightServiceFacadeTest {

    private FlightServiceFacade flightServiceFacade;
    private CrazyAirService crazyAirService;
    private ToughJetService toughJetService;
    private CommonTester commonTester;

    @Before
    public void init() {
        crazyAirService = mock(CrazyAirService.class);
        toughJetService = mock(ToughJetService.class);
        flightServiceFacade = new FlightServiceFacade(crazyAirService, toughJetService);
        commonTester = new CommonTester();
    }

    public List<FlightResponseDTO> getFlightResponses(FlightRequestDTO flightRequestDTO) {
        List<FlightService> flightServices = Arrays.asList(crazyAirService, toughJetService);
        return flightServices.
                parallelStream()
                .map(flightService -> {
                    return flightService.fetchFlightDetails(flightRequestDTO);
                })
                .flatMap(flightDetailsList -> flightDetailsList.stream())
                .collect(Collectors.toList());
    }


    @Test
    public void getFlightResponsesTest_Success() {
        List<FlightResponseDTO> flightResponseDTOsCrazyAir = commonTester.getFlightResponseDTOsCrazyAir();
        List<FlightResponseDTO> flightResponseDTOsToughJet = commonTester.getFlightResponseDTOsToughJet();
        doReturn(flightResponseDTOsCrazyAir)
                .when(crazyAirService)
                .fetchFlightDetails(any(FlightRequestDTO.class));
        doReturn(flightResponseDTOsToughJet)
                .when(toughJetService)
                .fetchFlightDetails(any(FlightRequestDTO.class));
        List<FlightResponseDTO> flightResponsesExpected = Stream.of(flightResponseDTOsCrazyAir, flightResponseDTOsToughJet)
                .flatMap(x -> x.stream())
                .collect(Collectors.toList());
        List<FlightResponseDTO> flightResponsesActual
                = flightServiceFacade.getFlightResponses(commonTester.getFlightRequestDTO());
        Assert.assertEquals(flightResponsesExpected, flightResponsesActual);
    }
}
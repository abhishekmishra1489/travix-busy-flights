package com.travix.medusa.busyflights.service.toughjet;


import com.travix.medusa.busyflights.CommonTester;
import com.travix.medusa.busyflights.circuitbreaker.FlightResilience;
import com.travix.medusa.busyflights.domain.FlightRequestDTO;
import com.travix.medusa.busyflights.domain.FlightResponseDTO;
import com.travix.medusa.busyflights.domain.toughjet.ToughJetRequest;
import com.travix.medusa.busyflights.domain.toughjet.ToughJetResponse;
import com.travix.medusa.busyflights.domain.toughjet.ToughJetResponseList;
import com.travix.medusa.busyflights.helper.toughjet.ToughJetServiceHelper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ToughJetServiceTest {

    private ToughJetService toughJetService;
    private FlightResilience flightResilience;
    private ToughJetServiceHelper toughJetServiceHelper;
    private CommonTester commonTester;

    @Before
    public void init() {
        toughJetServiceHelper = mock(ToughJetServiceHelper.class);
        flightResilience = mock(FlightResilience.class);
        toughJetService = new ToughJetService(flightResilience, toughJetServiceHelper);
        commonTester = new CommonTester();
    }


    public List<FlightResponseDTO> fetchFlightDetails(FlightRequestDTO flightRequestDTO) {
        ToughJetRequest toughJetRequest
                = toughJetServiceHelper.mapFlightRequestDtoToToughJetRequest(flightRequestDTO);
        ToughJetResponseList toughJetResponseList
                = flightResilience.callToughJetApi(toughJetRequest);
        return toughJetResponseList
                .getToughJetResponseList()
                .stream()
                .map(toughJetServiceHelper::mapToughJetResponseToFlightResponseDto)
                .collect(Collectors.toList());
    }

    @Test
    public void fetchFlightDetailsTest_success() {
        doReturn(mock(ToughJetRequest.class))
                .when(toughJetServiceHelper)
                .mapFlightRequestDtoToToughJetRequest(any(FlightRequestDTO.class));
        doReturn(commonTester.getToughJetResponseList())
                .when(flightResilience)
                .callToughJetApi(any(ToughJetRequest.class));
        doReturn(commonTester.getFlightResponseDTOToughJet())
                .when(toughJetServiceHelper)
                .mapToughJetResponseToFlightResponseDto(any(ToughJetResponse.class));
        List<FlightResponseDTO> flightResponseDTOSActual
                = toughJetService.fetchFlightDetails(commonTester.getFlightRequestDTO());
        assertEquals(Arrays.asList(commonTester.getFlightResponseDTOToughJet()), flightResponseDTOSActual);
    }

}
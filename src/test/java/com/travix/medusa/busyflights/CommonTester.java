package com.travix.medusa.busyflights;

import com.travix.medusa.busyflights.controller.request.BusyFlightsRequest;
import com.travix.medusa.busyflights.controller.response.BusyFlightsResponse;
import com.travix.medusa.busyflights.controller.response.BusyFlightsResponseList;
import com.travix.medusa.busyflights.domain.FlightRequestDTO;
import com.travix.medusa.busyflights.domain.FlightResponseDTO;
import com.travix.medusa.busyflights.domain.crazyair.CrazyAirRequest;
import com.travix.medusa.busyflights.domain.crazyair.CrazyAirResponse;
import com.travix.medusa.busyflights.domain.crazyair.CrazyAirResponseList;
import com.travix.medusa.busyflights.domain.enums.AirportCodeEnum;
import com.travix.medusa.busyflights.domain.enums.CabinClassEnum;
import com.travix.medusa.busyflights.domain.toughjet.ToughJetResponse;
import com.travix.medusa.busyflights.domain.toughjet.ToughJetResponseList;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;


public class CommonTester {

    public static LocalDateTime DEPARTURE_DATE = LocalDateTime.of(2019, 2, 24, 4, 30);
    public static LocalDateTime RETURN_DATE = LocalDateTime.of(2019, 3, 25, 5, 30);

    public BusyFlightsResponseList getBusyFlightsResponseListSortedByPrice() {
        BusyFlightsResponse busyFlightsResponse1 = BusyFlightsResponse.builder()
                .airline("Qatar Airways")
                .supplier("Tough Jet")
                .departureDate(DEPARTURE_DATE)
                .arrivalDate(RETURN_DATE)
                .fare(420.23)
                .departureAirportCode(AirportCodeEnum.AMS)
                .destinationAirportCode(AirportCodeEnum.MUC)
                .build();
        BusyFlightsResponse busyFlightsResponse2 = BusyFlightsResponse.builder()
                .airline("Qatar Airways")
                .supplier("Tough Jet")
                .departureDate(DEPARTURE_DATE)
                .arrivalDate(RETURN_DATE)
                .fare(423.23)
                .departureAirportCode(AirportCodeEnum.AMS)
                .destinationAirportCode(AirportCodeEnum.MUC)
                .build();
        BusyFlightsResponse busyFlightsResponse3 = BusyFlightsResponse.builder()
                .airline("Qatar Airways")
                .supplier("Crazy Air")
                .departureDate(DEPARTURE_DATE)
                .arrivalDate(RETURN_DATE)
                .fare(423.29)
                .departureAirportCode(AirportCodeEnum.AMS)
                .destinationAirportCode(AirportCodeEnum.MUC)
                .build();
        return BusyFlightsResponseList.builder()
                .busyFlightsResponseList(Arrays.asList(busyFlightsResponse1, busyFlightsResponse2 , busyFlightsResponse3))
                .build();
    }

    public List<FlightResponseDTO> getFlightResponseDTOsForSorting() {
        FlightResponseDTO flightResponseDTO1 = FlightResponseDTO.builder()
                .airline("Qatar Airways")
                .departureDate(CommonTester.DEPARTURE_DATE)
                .arrivalDate(CommonTester.RETURN_DATE)
                .departureAirportCode(AirportCodeEnum.AMS)
                .destinationAirportCode(AirportCodeEnum.MUC)
                .fare(423.29)
                .supplier("Crazy Air")
                .build();
        FlightResponseDTO flightResponseDTO2 = FlightResponseDTO.builder()
                .airline("Qatar Airways")
                .departureDate(CommonTester.DEPARTURE_DATE)
                .arrivalDate(CommonTester.RETURN_DATE)
                .departureAirportCode(AirportCodeEnum.AMS)
                .destinationAirportCode(AirportCodeEnum.MUC)
                .fare(423.23)
                .supplier("Tough Jet")
                .build();
        FlightResponseDTO flightResponseDTO3 = FlightResponseDTO.builder()
                .airline("Qatar Airways")
                .departureDate(CommonTester.DEPARTURE_DATE)
                .arrivalDate(CommonTester.RETURN_DATE)
                .departureAirportCode(AirportCodeEnum.AMS)
                .destinationAirportCode(AirportCodeEnum.MUC)
                .fare(420.23)
                .supplier("Tough Jet")
                .build();
        return Arrays.asList(flightResponseDTO1, flightResponseDTO2, flightResponseDTO3);
    }

    public BusyFlightsRequest getBusyFlightsRequestWithIncorrectTravelDates(LocalDateTime departureDateError, LocalDateTime returnDateError) {
        return BusyFlightsRequest.builder()
                .returnDate(returnDateError)
                .departureDate(departureDateError)
                .build();
    }

    public BusyFlightsRequest getBusyFlightsRequest() {
        return BusyFlightsRequest
                .builder()
                .departureDate(CommonTester.DEPARTURE_DATE)
                .returnDate(CommonTester.RETURN_DATE)
                .origin(AirportCodeEnum.AMS)
                .destination(AirportCodeEnum.MUC)
                .numberOfPassengers(2)
                .build();
    }

    public FlightRequestDTO getFlightRequestDTO() {
        return FlightRequestDTO.builder()
                .departureDate(DEPARTURE_DATE)
                .returnDate(RETURN_DATE)
                .origin(AirportCodeEnum.AMS)
                .destination(AirportCodeEnum.MUC)
                .numberOfPassengers(2)
                .build();
    }


    public BusyFlightsResponseList getBusyFlightsResponseListToughJet() {
        BusyFlightsResponse busyFlightsResponse = BusyFlightsResponse.builder()
                .airline("Qatar Airways")
                .supplier("Tough Jet")
                .departureDate(DEPARTURE_DATE)
                .arrivalDate(RETURN_DATE)
                .fare(600)
                .departureAirportCode(AirportCodeEnum.AMS)
                .destinationAirportCode(AirportCodeEnum.LHR)
                .build();
        return BusyFlightsResponseList.builder()
                .busyFlightsResponseList(Arrays.asList(busyFlightsResponse))
                .build();
    }


    public FlightResponseDTO getFlightResponseDTOCrazyAir() {
        return  FlightResponseDTO.builder()
                .airline("Cathay Pacific Airways")
                .supplier("Crazy Air")
                .departureDate(DEPARTURE_DATE)
                .arrivalDate(RETURN_DATE)
                .fare(700)
                .departureAirportCode(AirportCodeEnum.AMS)
                .destinationAirportCode(AirportCodeEnum.MUC)
                .build();
    }

    public List<FlightResponseDTO> getFlightResponseDTOsCrazyAir() {
        return Arrays.asList(getFlightResponseDTOCrazyAir());
    }
    public FlightResponseDTO getFlightResponseDTOToughJet() {
        return FlightResponseDTO.builder()
                .airline("Cathay Pacific Airways")
                .supplier("Tough Jet")
                .departureDate(CommonTester.DEPARTURE_DATE)
                .arrivalDate(CommonTester.RETURN_DATE)
                .fare(700)
                .departureAirportCode(AirportCodeEnum.AMS)
                .destinationAirportCode(AirportCodeEnum.MUC)
                .build();
    }
    public List<FlightResponseDTO> getFlightResponseDTOsToughJet() {
        return Arrays.asList(getFlightResponseDTOToughJet());
    }

    public CrazyAirRequest getCrazyAirRequest() {
        return  CrazyAirRequest.builder()
                .origin(AirportCodeEnum.AMS)
                .destination(AirportCodeEnum.MUC)
                .departureDate(CommonTester.DEPARTURE_DATE)
                .returnDate(CommonTester.RETURN_DATE)
                .passengerCount(2)
                .build();
    }

    public CrazyAirResponse getCrazyAirResponse() {
        return CrazyAirResponse.builder()
                .cabinClass(CabinClassEnum.B)
                .price(700)
                .departureDate(CommonTester.DEPARTURE_DATE)
                .arrivalDate(CommonTester.RETURN_DATE)
                .departureAirportCode(AirportCodeEnum.AMS)
                .destinationAirportCode(AirportCodeEnum.MUC)
                .airline("Cathay Pacific Airways")
                .build();
    }

    public CrazyAirResponseList getCrazyAirResponseList() {
        return CrazyAirResponseList.builder()
                .crazyAirResponseList(Arrays.asList(getCrazyAirResponse()))
                .build();
    }

    public ToughJetResponse getToughJetResponse() {
        return ToughJetResponse.builder()
                .departureAirportName(AirportCodeEnum.AMS)
                .arrivalAirportName(AirportCodeEnum.MUC)
                .outboundDateTime(CommonTester.DEPARTURE_DATE)
                .inboundDateTime(CommonTester.RETURN_DATE)
                .basePrice(700)
                .discount(10)
                .tax(4)
                .carrier("Cathay Pacific Airways")
                .build();
    }

    public ToughJetResponseList getToughJetResponseList() {
        return  ToughJetResponseList.builder()
                .toughJetResponseList(Arrays.asList(getToughJetResponse()))
                .build() ;
    }
}